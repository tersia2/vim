Personal vim configurations and plugins
---------------------------------------

This is a house for storing my personal ~/.vim settings;

Easier to syncronyze different working environments with these
shared between various computers.

Includes the extensions of Pathogen, NERDTree,
taglist, and snipMate.

