set nocompatible
call pathogen#infect()
call pathogen#helptags()

filetype plugin indent on
syntax on

set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent
set showmatch
set ruler
set incsearch
set backupdir=~/.vim/tmp
set directory=~/.vim/tmp

autocmd! BufNewFile * silent! 0r ~/.vim/templates/tmpl.%:e

if has("autocmd")
  augroup module
    autocmd BufRead,BufNewFile *.css set filetype=css
    autocmd BufRead,BufNewFile *.desktop set filetype=desktop
    autocmd BufRead,BufNewFile *.less set filetype=less
    autocmd BufWritePost *.less exe '!lessc ' . shellescape(expand('<afile>')) . ' > ' . shellescape(expand('<afile>:r')) . '.css'
  augroup END
endif

" Set TagList settings
let Tlist_Auto_Open = 0 " Don't auto-open.
let Tlist_Exit_OnlyWindow = 1 " exit if taglist is last window open
let Tlist_Show_One_File = 1 " Only show tags for current buffer
let Tlist_Use_Right_Window = 1 " Open on right side
let Tlist_Enable_Fold_Column = 0 " no fold column (only showing one file)
let tlist_sql_settings = 'sql;P:package;t:table'
let tlist_ant_settings = 'ant;p:Project;r:Property;t:Target'
let TList_Ctags_Cmd = "/usr/bin/ctags"
if has("gui_win32")
  let Tlist_Ctags_Cmd = 'C:/Users/tnanek/ctag58/ctags.exe' 
endif
let Tlist_Inc_Winwidth=0 " for konsole
let Tlist_WinWidth = 50

" Hotkey setup
map <F1> :NERDTreeToggle<CR>
map <F2> :TlistToggle<cr>
map <F3> :set expandtab<cr>
map <F4> :retab<cr>
